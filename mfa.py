#!/usr/bin/env python3

import hmac
import struct
import time
import base64
import json
from pathlib import Path

def get_hotp(key, cnt, digits=6):
    padded_key = key + '=' * (7 - (len(key) - 1) % 8)
    decoded_key = base64.b32decode(padded_key, casefold=True)
    data = hmac.digest(decoded_key, struct.pack('>Q', cnt), 'sha1')
    idx = data[-1] & 0xf
    num = (struct.unpack('>I', data[idx:idx+4])[0] & 0x7fff_ffff) % (10**digits)
    return f'{num:0{digits}}'

def get_totp(key, time_step=30, digits=6):
    cnt = int(time.time()) // time_step
    return get_hotp(key, cnt, digits)

def main():
    cfg = json.loads(Path('cfg.json').read_text())
    res = []
    for name, key_info in cfg['keys'].items():
        if not name:
            continue
        if isinstance(key_info, str):
            key_info = {'key': key_info}
        token = get_totp(key_info['key'], digits=key_info.get('digits', 6))
        res.append([name, token])
    max_name_len = max(len(x[0]) for x in res)
    for name, val in res:
        print(f'{name:>{max_name_len}}: {val}')

if __name__ == '__main__':
    main()
