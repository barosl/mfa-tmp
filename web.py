#!/usr/bin/env python3

from http.server import ThreadingHTTPServer, BaseHTTPRequestHandler
import subprocess

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()

        res = subprocess.run(['./mfa.py'], stdout=subprocess.PIPE)

        self.wfile.write(res.stdout)

if __name__ == '__main__':
    ThreadingHTTPServer(('', 8080), Handler).serve_forever()
