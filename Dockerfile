FROM python:alpine
WORKDIR /app
RUN ln -s /cfg/cfg.json
COPY *.py ./
CMD ["./web.py"]
