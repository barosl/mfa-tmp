#!/usr/bin/env python3

import argparse
from pathlib import Path
import base64

KEY_MASK = [57, 142, 39, 252, 80, 39, 106, 101, 96, 101, 176, 229, 37, 244, 192, 108, 4, 198, 16, 117, 40, 107, 142, 122, 237, 165, 157, 169, 129, 59, 93, 214, 200, 13, 47, 179, 128, 104, 119, 63, 165, 155, 164, 124, 23, 202, 108, 100, 121, 1, 92, 29, 91, 139, 143, 107, 154]

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('hash_file')
    args = ap.parse_args()

    text = Path(args.hash_file).read_text()
    encrypted_data = bytes.fromhex(text)
    assert len(encrypted_data) == len(KEY_MASK) == 57
    data = bytes(a ^ b for a, b in zip(encrypted_data, KEY_MASK)).decode('ascii')

    key_raw, key_desc = bytes.fromhex(data[:40]), data[40:]
    key_encoded = base64.b32encode(key_raw).decode('ascii')
    print(f'{key_desc}: {key_encoded}')

if __name__ == '__main__':
    main()
